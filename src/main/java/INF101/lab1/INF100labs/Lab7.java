package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs


        ArrayList<ArrayList<Integer>> grid = new ArrayList<>();
        grid.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid.add(new ArrayList<>(Arrays.asList(6, 8, 1)));
        System.out.println(allRowsAndColsAreEqualSum(grid));
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {


        grid.remove(row);

        System.out.println(grid);


        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        //throw new UnsupportedOperationException("Not implemented yet.");

        ArrayList<Integer> rowSumsList = new ArrayList<>(Collections.nCopies(grid.size(), 0));
        ArrayList<Integer> colSumsList = new ArrayList<>(Collections.nCopies(grid.size(), 0));


        for (int i = 0; i < grid.size(); i++) {

            ArrayList<Integer> innerList = new ArrayList<>(grid.get(i));

            int rowSums = 0;

            for (int j = 0; j < grid.size(); j++) {

                rowSums += innerList.get(j);
                colSumsList.set(j, colSumsList.get(j) + innerList.get(j));
                
            }
            
            rowSumsList.set(i, rowSums);
        
        }

        ArrayList<Integer> rowSumsFinalList = new ArrayList<>();
        ArrayList<Integer> colSumsFinalList = new ArrayList<>();

        for (int i : rowSumsList) {
            if (!rowSumsFinalList.contains(i)) {
                rowSumsFinalList.add(i);
            }
        }

        for (int i : colSumsList) {
            if (!colSumsFinalList.contains(i)) {
                colSumsFinalList.add(i);
            }
        }

        if (colSumsFinalList.size() == 1 && rowSumsFinalList.size() == 1){
            return true;
        }
        else {
            return false;
        }
    }
}
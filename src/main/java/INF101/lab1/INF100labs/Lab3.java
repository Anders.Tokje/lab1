package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        // multiplesOfSevenUpTo(50);
        // multiplicationTable(5);
        
        System.out.println(crossSum(34));
    }

    public static void multiplesOfSevenUpTo(int n) {
        // throw new UnsupportedOperationException("Not implemented yet.");

        int seven = 0;

        while (n % 7 != 0) {
            n -= 1;
        }

        while (seven != n) {
            seven += 7;
            System.out.println(seven);
        }
    }

    public static void multiplicationTable(int n) {
        // throw new UnsupportedOperationException("Not implemented yet.");

        for (int i = 1; i <= n; i++) {


            String outterString = String.format("%d:", i);
            for (int j = 1; j <= n; j++) {

                int newInt = j*i;
                String innerString = String.format(" %d", newInt);
                outterString += innerString;
                
            }
            System.out.println(outterString);
        }
    }

    public static int crossSum(int num) {
        //throw new UnsupportedOperationException("Not implemented yet.");

        String stringedInt = String.format("%d", num);

        int sum = 0;

        for (int i = 0; i < stringedInt.length(); i++) {
            char c = stringedInt.charAt(i);
            int number = Character.getNumericValue(c);
            sum += number;
        }

        return sum;
    }

}
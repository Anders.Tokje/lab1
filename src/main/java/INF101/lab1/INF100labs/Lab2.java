package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("Four", "Five", "Nine");
    }

    public static void findLongestWords(String word1, String word2, String word3) {

        List<String> nyListe = new ArrayList<String>(Arrays.asList(word1, word2, word3));
        int longestString = 0;

        List<String> longestStringList = new ArrayList<String>();

        for (String i : nyListe) {
            if (i.length() > longestString) {
                longestString = i.length();
                longestStringList.clear();
                longestStringList.add(i);
            }
            
            if ((i.length() == longestString) && (!longestStringList.contains(i))) {
                longestStringList.add(i);
            }
           
        }

        for (String i : longestStringList) {
            System.out.println(i);
        }
        // throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
        // throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0) {
            return true;
        } 
        else {
            return false;
        }
        // throw new UnsupportedOperationException("Not implemented yet.");
    }

}

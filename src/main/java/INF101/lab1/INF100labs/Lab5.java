package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        // ArrayList<Integer> intList1 = new ArrayList<>(Arrays.asList(1,2,3));
        // ArrayList<Integer> intList2 = new ArrayList<>(Arrays.asList(1,2,3));

        // addList(intList1, intList2);
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        for (int i = 0; i < list.size(); i++) {

            int sum = list.get(i) * 2;
            list.set(i, sum);

            
        }
        return list;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        for (int i = 0; i < list.size(); i++) {

            if (list.get(i) == 3) {
                list.remove(i);
                i -= 1;
            }
        }
        return list;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        // throw new UnsupportedOperationException("Not implemented yet.");
        ArrayList<Integer> tempList = new ArrayList<>();

        for (int i : list) {
            if (!tempList.contains(i)) {
                tempList.add(i);
            }
        }
        return tempList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        //throw new UnsupportedOperationException("Not implemented yet.");

        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
        System.out.println(a);
    }

}